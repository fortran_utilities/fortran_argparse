# fortran_argparse

[![pipeline status](https://gitlab.com/fortran_utilities/fortran_argparse/badges/main/pipeline.svg)](https://gitlab.com/fortran_utilities/fortran_argparse/-/commits/main)

A simple implemention of a vaguely useful argument parser for fortran.
It doesn't try to be efficient, scalable or complete but hopefully
covers some common use cases.

# Requirements

* Fortran 2008 compliant compiler
* Cmake

# Building

To build the library you will need to run something like

```bash
cmake . -B build && cmake --build build
```

One could the make use of the library in a custom project
`example.f90` using something like

```bash
<fortran_compiler> -Lbuild -Ibuild/mod example.f90 -lfortran_argparse
```

# Testing

The code comes with pFUnit tests. To build and run these one can run

```bash
cmake . -B build -DFARGPARSE_BUILD_TESTS=ON && cmake --build build --target check
```

If you do not have a copy of pFUnit available then we can
automatically download and build this using

```bash
cmake . -B build -DFARGPARSE_BUILD_TESTS=ON -DFARGPARSE_DOWNLOAD_PFUNIT=ON && cmake --build build --target check
```

# Documentation

The FORD documentation system is used to provide documentation gathered from code comments and supplementary markdown.
To build the documentation you need to enable and specify the documentation target, e.g.

```bash
cmake . -B build -DFARGPARSE_BUILD_DOCUMENTATION=ON && cmake --build build --target documentation
```

This will create the documentation website in `build/documentation`. Alternatively one may build directly in the documentation directory as

```bash
cd documentation
cmake . -B build && cmake --build build --target documentation
```

to produce the documentation directly in `build`.

A version of the documentation is hosted [here](https://fortran_utilities.gitlab.io/fortran_argparse/).

# Examples

Some example uses will be provided. To build these configure with
`FARGPARSE_BUILD_EXAMPLES` set to `ON`. For example

```bash
cmake . -B build -DFARFPARSE_BUILD_EXAMPLES=ON && cmake --build build
```

will configure and build the examples. They can then be found in
`build/examples/`.

