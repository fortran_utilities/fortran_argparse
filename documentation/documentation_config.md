---
project: Fortran Argparse
project_gitlab: https://gitlab.com/fortran_utilities/fortran_argparse
project_download: https://gitlab.com/fortran_utilities/fortran_parse/-/archive/main/fortran_argparse-main.tar.gz
summary:
	A small toy project to implement a crude argument parser for
	 fortran.  It is not intended to be efficient,
	flexible or complete.
author: D Dickinson
author_description:
	Computational physicist based at the University of York
parallel: 16
src_dir: ../src
css: user.css
output_dir: ./html
graph_dir: ./graphs
page_dir: ./pages
media_dir: ./media
fpp_extensions: F90
predocmark: >
predocmark_alt: !
docmark: <
docmark_alt: #
display: public
display: protected
display: private
source: true
print_creation_date: true
search: false
graph: false
coloured_edges: true
---

{!../README.md!}
