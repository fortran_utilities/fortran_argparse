# Adds target for pfunit tests in test_source
# name fargparse_tests_${test_name} and adds to
# list of know tests cases to give to ctest.
function(fargparse_add_test test_source test_name)
  add_pfunit_test(fargparse_tests_${test_name}
    "${test_source}" "" "")
  target_link_libraries(fargparse_tests_${test_name} fortran_argparse)
  list(APPEND FARGPARSE_CTEST_CASES fargparse_tests_${test_name})
  set(FARGPARSE_CTEST_CASES ${FARGPARSE_CTEST_CASES} PARENT_SCOPE)
endfunction()

# Helper function to easily add multiple separate
# tests provided they exist at tests/test_${name}.pf
# and we're happy to identify them as fargparse_tests_${name}
function(fargparse_add_tests)
  cmake_parse_arguments(
	FARGPARSE_ADD "" "" "TEST_NAMES" ${ARGN}
    )
  foreach(name ${FARGPARSE_ADD_TEST_NAMES})
    fargparse_add_test("test_${name}.pf" ${name})
  endforeach()
  set(FARGPARSE_CTEST_CASES ${FARGPARSE_CTEST_CASES} PARENT_SCOPE)
endfunction()

